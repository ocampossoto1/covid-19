import React from 'react';
import { Container } from 'reactstrap';
import NavMenu from './NavMenu';
// import ToolBar from './ToolBar';


class Layout extends React.Component {
  static displayName = Layout.name;
  
  render() {
  return <div>
     <NavMenu />
     
      <Container style={{height: "90vh", paddingLeft: "4%", paddingRight: "4%"}}>
        {this.props.children}
         
      </Container>
      
      
    </div>
  }
};


export default Layout;