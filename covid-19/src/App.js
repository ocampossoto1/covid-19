import 'bootstrap/dist/css/bootstrap.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import React from 'react';
import './App.css';
import Layout from './Layout';
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Covid from './Components/OldCode/covid'
import CovidUS from './Components/CovidUS/USCovid'
import firebase from "firebase/app";
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/analytics';
var firebaseconfig = {
  apiKey: "AIzaSyB6t6ED-8R1CWbt-R4xjZhy5RE1CFbc9R0",
  authDomain: "just-scout.firebaseapp.com",
  databaseURL: "https://just-scout.firebaseio.com",
  projectId: "just-scout",
  storageBucket: "just-scout.appspot.com",
  messagingSenderId: "442845763920",
  appId: "1:442845763920:web:bb53c9c8022167c6573455",
  measurementId: "G-CPZJXFGW4R"
}
firebase.initializeApp(firebaseconfig);
class App extends React.Component{

  render(){
    return (
      <Layout>
        <Switch>
          <Route exact path='/Old' component={Covid} />
          <Route exact path='/CovidUS' component={CovidUS}/>
          <Redirect exact from="/" to="CovidUS" />
        </Switch>
      </Layout>
    )
  }
}

export default App;
