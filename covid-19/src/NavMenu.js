import React from 'react';
import {
  Collapse,
  Container, 
  Navbar, 
  NavbarBrand, 
  NavbarToggler, 
  NavItem, 
  NavLink
} from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';

class NavMenu extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      isOpen: false,
      dropDown: false,
    };
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  toggledrop(){
    this.setState({
      dropDown: !this.state.dropDown
    }) 
  }
  CloseNavBar = () =>{
    if(this.state.isOpen){
      this.toggle();
    }
  }
  render () {
    return (
      <header>
        <Navbar className="navbar-dark bg-dark navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-2">
          <Container>
            <NavbarBrand tag={Link} to="/Old">Covid</NavbarBrand>
            <NavbarToggler type="dark" onClick={this.toggle} className="mr-3" />
            
            <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={this.state.isOpen} navbar>
              <ul className="navbar-nav flex-grow">
                <NavItem>
                  <NavLink tag={Link} to="/Old">Covid World</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} onClick={this.CloseNavBar} className="" to="/CovidUS">Covid US</NavLink>
                </NavItem>
               </ul>
            </Collapse>
          </Container>
        </Navbar> 
      </header>
    );
  }
}
export default NavMenu;