import React from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis,Tooltip,Legend, ResponsiveContainer} from 'recharts';
class CovidState extends React.Component {


    render(){
        return( 
        <ResponsiveContainer maxHeight="40%" style={{marginLeft: "0%"}}>
            <LineChart data={this.props.data}>
                <Line type="monotone" dataKey={this.props.item.name} stroke={this.props.item.color} />
                <CartesianGrid stroke="#ccc" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip/>
                <Legend />
            </LineChart>
        </ResponsiveContainer>
        )
    }
}

export default CovidState;