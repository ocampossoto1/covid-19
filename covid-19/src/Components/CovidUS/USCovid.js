import React from 'react';
// import NumberFormat from 'react-number-format';
import CovidState from './State';
import Select from 'react-select';
import {Tab, Tabs } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import {Row, Col} from 'react-bootstrap';
import CovidCountry from './Country';
import { Label } from 'reactstrap';
const datesAreOnSameDay = (first, second) =>
    first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate();
class CovidUS extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            allData: {}, keys: [], options: {}, loading: false, key: "Select a State", multiValue: [],exclude:[],
            DataTypes: [
                {"name": "Confirmed", "color": "Blue"},
                {"name": "Confirmed Per Day", "color":"LightBlue"},
                {"name": "Recovered", "color": "Green"},
                {"name": "Recovered Per Day", "color": "LightGreen"},
                {"name": "Deaths", "color": "DarkRed"},
                {"name": "Deaths Per Day", "color":"Red"}
            ],
            columns: [{
                dataField: 'state',
                text: 'State',
                sort: true
              }, {
                dataField: 'Confirmed',
                text: 'Confirmed',
                sort: true
              }, {
                dataField: 'Deaths',
                text: 'Deaths',
                sort: true
              }, {
                dataField: 'Recovered',
                text: 'Recovered',
                sort: true
              }

            ]
        }
    }
  componentDidMount(){
    this.GetData();
  }
  GetData = ()=>
  {
    var url = "https://firebasestorage.googleapis.com/v0/b/frcscout-6d1d3.appspot.com/o/covid%2FCovidData.json?alt=media&token=743d535c-3eb2-49f8-8579-8f93589d076e";
    fetch(url).then(res=> res.json()).then((result)=>{  
        var keys = Object.keys(result["US"]);
        var TempData = []
        keys.forEach(element => {
            TempData.push({value: element, label: element});
        })
        TempData.push({value: "Select a State", label: "Select a State"})
        this.setState({ allData: result['US'], keys: keys, options: TempData, loading: false, key: this.state.key}, () =>this.UpdateData());
    }).catch((error) => {
        console.log(error)
      });
    
  }
  UpdateData = () =>{
      this.setState({data:this.getData(this.state.allData[this.state.key]), tableData: this.converDataCurrent(this.state.allData)});
      this.GetCumulative();
  }
  getData=(data)=>{
    var ResultData = []
    for(var i in data){
        var element = {};
        if(data[i].Confirmed > 0){
            element["name"] = i;
            element["Recovered"] = data[i].Recovered !== "" ? parseInt(data[i].Recovered, 10) : 0 ;
            element["Confirmed"] = data[i].Confirmed !== "" ? parseInt(data[i].Confirmed, 10) : 0 ;
            element["Deaths"] = data[i].Deaths!== "" ? parseInt(data[i].Deaths, 10) : 0 ;
            if(ResultData[ResultData.length-1]!==undefined){
                element["Deaths Per Day"] = data[i].Deaths!== "" ? parseInt(data[i].Deaths, 10) - parseInt(ResultData[ResultData.length-1].Deaths, 10) : 0;
                element["Confirmed Per Day"] = data[i].Confirmed!== "" ? parseInt(data[i].Confirmed, 10) - parseInt(ResultData[ResultData.length-1].Confirmed, 10) : 0;
                element["Recovered Per Day"] = data[i].Recovered!== "" ? parseInt(data[i].Recovered, 10) - parseInt(ResultData[ResultData.length-1].Recovered, 10) : 0;
            }
            else{
                element["Deaths Per Day"] = 0;
                element["Confirmed Per Day"] = 0;
                element["Recovered Per Day"] = 0;
            }
        }
        else{
            element["Deaths Per Day"] = 0;
            element["Confirmed Per Day"] = 0;
            element["Recovered Per Day"] = 0;
        }
        ResultData.push(element);
    }
    return ResultData;
  }
  converDataCurrent=(data)=>{
    var results = [];
    var keys = this.state.keys;
    for(var i in keys){
        var element = {};
        for(var day in data[keys[i]]){
            var date = new Date(day);
            if(datesAreOnSameDay(date, new Date(Date.now()))){
                element = data[keys[i]][day];
                element['state'] = keys[i]; 
                element["Recovered"] = element['Recovered'] !== "" ? parseInt(element['Recovered'], 10) : 0 ;
                element["Confirmed"] = element['Confirmed'] !== "" ? parseInt(element['Confirmed'], 10) : 0 ;
                element["Deaths"] = element['Deaths'] !== "" ? parseInt(element['Deaths'], 10) : 0 ;
                element['Testing_Rate'] = element['Testing_Rate'] !=="" ? parseFloat(element['Testing_Rate']).toFixed(3): 0.00;
                element['Mortality_Rate'] = element['Mortality_Rate'] !=="" ? parseFloat(element['Mortality_Rate']).toFixed(3): 0.00;
            }
        }
        results.push(element);
        
    }
    return results;
  }
  GetCumulative=()=>{
    var results = {};
    var keys = this.state.keys;
    var all = this.state.allData;
    for(var i in keys){
        if(!this.state.exclude.includes(keys[i])){
            for(var day in all[keys[i]]){
                var date = new Date(day);
                date = date.getMonth()+1+"/"+date.getDate()+"/"+date.getFullYear();
                if(day !==""){
                    if (!(date in results)){
                        results[date] = {
                            'Confirmed': all[keys[i]][day]['Confirmed'] !== "" ? parseInt(all[keys[i]][day]['Confirmed']): 0, 
                            'Deaths': all[keys[i]][day]['Confirmed'] !== "" ? parseInt(all[keys[i]][day]['Confirmed']): 0, 
                            'Recovered': all[keys[i]][day]['Recovered'] !== "" ? parseInt(all[keys[i]][day]['Confirmed']): 0,
                            "Deaths Per Day" : 0,
                            "Confirmed Per Day" : 0,
                            "Recovered Per Day" :  0
                        };
                    }
                    else{
                        results[date]['Confirmed'] += all[keys[i]][day]['Confirmed']!== ""? parseInt(all[keys[i]][day]['Confirmed']): 0;
                        results[date]['Deaths']+= all[keys[i]][day]['Deaths'] !== ""? parseInt(all[keys[i]][day]['Deaths']): 0;
                        results[date]['Recovered']+= all[keys[i]][day]['Recovered'] !== ""? parseInt(all[keys[i]][day]['Recovered']): 0;                    
                    }
                }
            }
        }
    }
    var finalResults = []
    for(var key in results){
        date = new Date(key);
        var prevDate = new Date(date.setDate(date.getDate()-1));
        prevDate = prevDate.getMonth()+1+"/"+prevDate.getDate()+"/"+prevDate.getFullYear();
        if(prevDate in results){
            results[key]["Deaths Per Day"] = results[key]['Deaths']- results[prevDate]['Deaths'];
            results[key]["Recovered Per Day"] = results[key]['Recovered']- results[prevDate]['Recovered'];
            results[key]["Confirmed Per Day"] = results[key]['Confirmed']- results[prevDate]['Confirmed'];
        }
        var element = results[key];
        element['name'] = key;
        finalResults.push(element);
    }
    this.setState({Cumulative: finalResults});
  }
  handleMultiChange=(option)=> {
    var values = [];
    for(var i in option){
        values.push(option[i].value);
    }
    this.setState({multiValue: option, exclude: values}, ()=> this.GetCumulative());
  }
  render(){
      return (<>
        <Tabs defaultActiveKey="Table" id="uncontrolled-tab-example" onSelect={(e) => {this.setState({show: e})}}>
            <Tab eventKey="Table" title="Table">
                {this.state.tableData ? <BootstrapTable 
                    keyField='state' 
                    data={ this.state.tableData }  
                    columns={ this.state.columns } 
                    rowStyle={{ textAlign: "center",wordWrap: "break-word" }} 
                    striped 
                 />: null}
            </Tab>
            <Tab eventKey="Graph" title="Graph By State">
               <Row>
                   <Col>
                        <Select
                            onChange={(selectedOption ) => this.setState({key:selectedOption.value}, () =>this.UpdateData())}
                            options={this.state.options}
                            isLoading={this.state.loading}
                            value={{label: this.state.key}}
                            isSearchable/>
                    </Col>
               </Row>
            </Tab>
            <Tab eventKey="Graph US" title="Graph US">
                <Row>
                    <Col><Label>Exclude States: </Label></Col>
                    <Col>
                        <Select
                            onChange={(e) => {this.handleMultiChange(e);}}
                            options={this.state.options}
                            isLoading={this.state.loading}
                            // value={{label: this.state.multiValue}}
                            isMulti
                            isSearchable/>
                    </Col>
                </Row>
                
                </Tab>
        </Tabs>
        {this.state.show ==="Graph" && this.state.key !=="Select a State"  ? this.state.DataTypes.map(item => 
            <CovidState key={item.name} data={this.state.data} item={item}/>): null
        }
         {this.state.show ==="Graph US" ? this.state.DataTypes.map(item => 
            <CovidCountry key={item.name} data={this.state.Cumulative} item={item}/>): null
        }
        
        </>
      )
  }
}

export default CovidUS;